#!/bin/bash

set -e
usage()
{
cat << EOF
usage: $0 options

OPTIONS:
   -h      Print this msg
EOF
}

while getopts h opts; do
  case "$opts" in
    h) usage; exit 1 ;;
    ?) usage; exit 1 ;;
  esac
done

ansible-playbook ansible/deploy.yml --vault-password-file=~/.vault_passwd.txt -i 44.228.149.92, --private-key ~/.ssh/ubntmgmt.pem 
