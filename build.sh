#!/bin/bash

set -e
usage()
{
cat << EOF
usage: $0 options

OPTIONS:
   -s      Service name to build (doggif) 
   -h      Print this msg
EOF
}

if [ $# -eq 0 ]; then
  usage; exit 0
fi

while getopts hs: opts; do
  case "$opts" in
    s) export SERVICE=$OPTARG ;;
    h) usage; exit 1 ;;
    ?) usage; exit 1 ;;
  esac
done

docker build ./$SERVICE -t bmcgair/$SERVICE:latest
# Use personal access token for login 
docker login --username=bmcgair --password=9f3bbf19-3b08-43c4-a88d-1ac6ede0b815
docker push bmcgair/$SERVICE:latest
